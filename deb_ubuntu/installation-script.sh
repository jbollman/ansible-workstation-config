#!/bin/bash

# no password required
sed -i '/%sudo	ALL=(ALL:ALL) ALL/c\%sudo	ALL=(ALL:ALL) NOPASSWD: ALL' /etc/sudoers

#TBD
#We need to install ssh, (not default on desktop)
sudo apt update && sudo apt install ssh

# add software-properties-common package (necessary for add-apt command)
sudo apt install software-properties-common

# create ssh key for ansible
ssh-keygen -t rsa -q -f "$HOME/.ssh/id_rsa" -N ""

# copy key over to localhost
#ssh-copyid localhost

# ssh-copy id is interactive, will have to use ssh pass for batch mode
#sshpass -p qq ssh-copy-id ansible@localhost


# INSTALL ANSIBLE
# ------------

# ADD PPA
 sudo apt-add-repository ppa:ansible/ansible

# Install portion
sudo apt update -y
sudo apt install ansible -y

# edit inventory
sudo sed -i '/#inventory      = \/etc\/ansible\/hosts/c\inventory      = \/etc\/ansible\/hosts' /etc/ansible/ansible.cfg

# backup original /etc/ansible/hosts
sudo mv /etc/ansible/hosts /etc/ansible/hosts.bak

echo -e "[localhost]\nlocalhost" > /etc/ansible/hosts
