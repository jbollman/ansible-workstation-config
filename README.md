# ansible-workstation-config
To configure a workstation after a fresh new install of Linux.

 Works fine on 17.10 / 16.04
 Does not work on 18.04. Most packages do not have canidate relase version yet for latest LTS

#Packages and Apps that get installed

--- Common Role
- Latest Ansible version
- vim, htop, wget, git, unip, curl, tree, sublime-text-installer
- downloads a vimrc file
--- Developer Role
- vagrant
- virtualbox
- groovy
- packer
- ChefDK

--- Extras Role
- snapd
- discord
- spotify


# Packages to be installed in the future
- Docker
- kubernetes
- slack (snapd install is buggy)
- evernote client
