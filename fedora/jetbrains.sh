#!/bin/bash

mkdir -p ~/bin/

export PATH=$PATH:~/bin

cd ~/bin

if [ ! -e "./jetbrains-toolbox"* ]
then
  wget --no-check-certificate https://download.jetbrains.com/toolbox/jetbrains-toolbox-1.12.4481.tar.gz; tar -xvf jetbrain*;
else
 echo "jetbrains toolbox tar.gz already downloaded"
fi

